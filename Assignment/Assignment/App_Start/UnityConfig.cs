using Assignment.Models;
using Assignment.Services;
using System.Web.Mvc;
using Unity;
using Unity.Mvc5;

namespace Assignment
{
    public static class UnityConfig
    {
        public static void RegisterComponents()
        {
			var container = new UnityContainer();

            // register all your components with the container here
            // it is NOT necessary to register your controllers

            // e.g. container.RegisterType<ITestService, TestService>();
            container.RegisterType<IAssignmentContext, AssignmentContext>();
            container.RegisterType<ISectorService, SectorService>();

            DependencyResolver.SetResolver(new UnityDependencyResolver(container));
        }
    }
}