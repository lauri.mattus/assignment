﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Assignment.Models
{
    public class SectorInvolvement
    {
        [Key, Column(Order = 0)]
        public int SectorId { get; set; }
        [Key, Column(Order = 1)]
        public int InvolvementId { get; set; }
        public Sector Sector { get; set; }
        public Involvement Involvement { get; set; }

    }
}