﻿using Assignment.Migrations;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace Assignment.Models
{
    public class AssignmentContext : DbContext, IAssignmentContext
    {
        // You can add custom code to this file. Changes will not be overwritten.
        // 
        // If you want Entity Framework to drop and regenerate your database
        // automatically whenever you change your model schema, please use data migrations.
        // For more information refer to the documentation:
        // http://msdn.microsoft.com/en-us/data/jj591621.aspx
    
        public AssignmentContext() : base("name=AssignmentContext")
        {
            Database.SetInitializer(new MigrateDatabaseToLatestVersion<AssignmentContext, Configuration>());
        }

        //protected override void OnModelCreating(DbModelBuilder modelBuilder)
        //{
        //    modelBuilder.Entity<Sector>().
        //      HasOptional(e => e.Parent).
        //      WithMany().
        //      HasForeignKey(m => m.ParentID);
        //}

        public System.Data.Entity.DbSet<Assignment.Models.Sector> Sectors { get; set; }
        public System.Data.Entity.DbSet<Assignment.Models.SectorInvolvement> SectorInvolvements { get; set; }

        public System.Data.Entity.DbSet<Assignment.Models.Involvement> Involvements { get; set; }

        public void MarkAsModified(Sector item)
        {
            Entry(item).State = EntityState.Modified;
        }
    }
}
