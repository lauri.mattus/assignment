﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Assignment.Models
{
    public class InvolvementViewModel
    {
        
        public int InvolvmentId { get; set; }
        [Required()]
        public string Name { get; set; }
        [Display(Name = "Agree To Terms")]
        [MustBeTrue(ErrorMessage = "You have to Agree To Terms to save data.")]
        public bool AgreedToTerms { get; set; }
        [Required(ErrorMessage ="Select at least one Sector")]
        public int[] SelectedSectorIds { get; set; }
        public ICollection<Sector> Sectors { get; set; }

    }
}