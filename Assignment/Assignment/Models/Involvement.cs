﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Assignment.Models
{
    public class Involvement
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public bool AgreedToTerms { get; set; }
        public ICollection<SectorInvolvement> SectorInvolvements { get; set; }
    }
}