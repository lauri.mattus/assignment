﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Assignment.Models
{
    public class Sector
    {
        public int Id { get; set; }
        [Required()]
        public string Name { get; set; }
        [ForeignKey("Parent")]
        public int? ParentId { get; set; }
        public virtual Sector Parent { get; set; }
        public ICollection<SectorInvolvement> SectorInvolvements { get; set; }
        public ICollection<Sector> Children { get; set; }

     

    }


}