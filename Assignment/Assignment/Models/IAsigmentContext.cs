﻿using System.Data.Entity;

namespace Assignment.Models
{
    public interface IAssignmentContext
    {
        DbSet<Sector> Sectors { get; }
        DbSet<SectorInvolvement> SectorInvolvements { get; }
        DbSet<Involvement> Involvements { get; }
        int SaveChanges();
        void MarkAsModified(Sector item);
        void Dispose();
    }
}