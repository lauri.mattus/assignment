﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Assignment.Services;
using Assignment.Models;

namespace Assignment.Controllers
{
    public class SectorsController : Controller
    {
        private IAssignmentContext db;

        private ISectorService sectorService;


        public SectorsController(IAssignmentContext assignmentContext, ISectorService sectorService)
        {
            this.db = assignmentContext;
            this.sectorService = sectorService;
        }
        // GET: Sectors
        public ActionResult Index()
        {
            var sectors = db.Sectors.Include(s => s.Parent);
            return View(sectors.ToList());
        }

        // GET: Sectors/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Sector sector = db.Sectors.Find(id);
            if (sector == null)
            {
                return HttpNotFound();
            }
            return View(sector);
        }

        // GET: Sectors/Create
        public ActionResult Create()
        {
            var sectors = db.Sectors.ToList();
            sectors.Insert(0, null);
            ViewBag.ParentId = new SelectList(sectors, "Id", "Name");
            return View();
        }

        // POST: Sectors/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,Name,ParentId")] Sector sector)
        {
            if (ModelState.IsValid)
            {
                db.Sectors.Add(sector);
                db.SaveChanges();
                sectorService.AddToCache(HttpContext, sectorService.RetrieveSectors());
                return RedirectToAction("Index");
            }

            var sectors = db.Sectors.ToList();
            sectors.Insert(0, new Sector());
            ViewBag.ParentId = new SelectList(sectors, "Id", "Name", sector.ParentId);
            return View(sector);
        }

       

        // GET: Sectors/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Sector sector = db.Sectors.Find(id);
            if (sector == null)
            {
                return HttpNotFound();
            }
            var sectors = db.Sectors.ToList();
            sectors.Insert(0, new Sector());
            ViewBag.ParentId = new SelectList(sectors, "Id", "Name", sector.ParentId);
            return View(sector);
        }

        // POST: Sectors/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,Name,ParentId")] Sector sector)
        {
            if (ModelState.IsValid)
            {
                if (sector.ParentId == 0)
                    sector.ParentId = null;
                db.MarkAsModified(sector);
                db.SaveChanges();
                sectorService.AddToCache(HttpContext, sectorService.RetrieveSectors());
                
                return RedirectToAction("Index");
            }
            var sectors = db.Sectors.ToList();
            sectors.Insert(0, new Sector());
            ViewBag.ParentId = new SelectList(sectors, "Id", "Name", sector.ParentId);
            return View(sector);
        }

        // GET: Sectors/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            // Check if we have involvements connected to this sector
            if (InvolvementExists(id))
            {
                TempData["CannotDelete"] = "Cannot delete Sector that has involvements";
                return RedirectToAction("Index");
            }

            if (HasChildSectors(id))
            {
                TempData["CannotDelete"] = "Cannot delete Sector that has Sub-Sectors";
                return RedirectToAction("Index");
            }

            Sector sector = db.Sectors.Find(id);

            if (sector == null)
            {
                return HttpNotFound();
            }
            return View(sector);
        }

        private bool HasChildSectors(int? id)
        {
            return db.Sectors.FirstOrDefault(x => x.ParentId == id) != null;
        }

        private bool InvolvementExists(int? id)
        {
            return db.SectorInvolvements.FirstOrDefault(x => x.SectorId == id) != null;
        }

        // POST: Sectors/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Sector sector = db.Sectors.Find(id);
            db.Sectors.Remove(sector);
            db.SaveChanges();
            HttpContext.Application["Sectors"] = sectorService.RetrieveSectors();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
