﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;
using System.Web.Mvc;
using Assignment.Models;
using Assignment.Services;

namespace Assignment.Controllers
{
    public class InvolvementsController : Controller
    {
        private ISectorService sectorService;

        public InvolvementsController(ISectorService sectorService)
        {
            this.sectorService = sectorService;
        }

        // GET: Involvements
        public ActionResult Index()
        {
            InvolvementViewModel involvementViewModel;

            if (!sectorService.ExistsInSession(Session))
            {
                involvementViewModel = new InvolvementViewModel();
            }
            else
            {
                involvementViewModel = sectorService.RetrieveFromSession(Session);
            }

            involvementViewModel.Sectors = new List<Sector>();
            sectorService.AddSectorsToViewModel(involvementViewModel, HttpContext);

            return View(involvementViewModel);
        }
        
        // POST: Involvements
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Index([Bind(Include = "Id,Name,AgreedToTerms,SelectedSectorIds")] InvolvementViewModel involvementViewModel)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    Involvement involvement = null;


                    involvement = sectorService.RetrieveInvolvement(involvement, Session);

                    involvement.Name = involvementViewModel.Name;
                    involvement.AgreedToTerms = involvementViewModel.AgreedToTerms;
                    involvement.SectorInvolvements = new List<SectorInvolvement>();

                    // Remove old Sector Involvements
                    if (involvement.Id > 0)
                    {
                        sectorService.RemoveExistingInvolvements(involvement);
                    }

                    // Add new Sector Involvements
                    foreach (var sectorId in involvementViewModel.SelectedSectorIds)
                    {
                        var sectorInvolvement = new SectorInvolvement()
                        {
                            SectorId = sectorId,
                        };
                        involvement.SectorInvolvements.Add(sectorInvolvement);
                    }

                    sectorService.Save(involvement);
                    ViewData["Saved"] = "Saved successfully";
                    involvementViewModel.InvolvmentId = involvement.Id;
                }
                catch (Exception ex)
                {
                    ViewData["Saved"] = "Saving failed";
                }
            }

            sectorService.AddToCache(involvementViewModel, Session);
            sectorService.AddSectorsToViewModel(involvementViewModel, HttpContext);

            return View(involvementViewModel);
        }

    }
}
