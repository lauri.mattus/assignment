﻿using System.Collections.Generic;
using System.Web;
using Assignment.Models;

namespace Assignment.Services
{
    public interface ISectorService
    {
        void AddSectorsToViewModel(InvolvementViewModel involvementViewModel, HttpContextBase context);
        void AddToCache(HttpContextBase context, ICollection<Sector> sectors);
        void AddToCache(InvolvementViewModel involvementViewModel, HttpSessionStateBase session);
        bool ExistsInSession(HttpSessionStateBase session);
        bool NotExistInCache(HttpContextBase context);
        void RemoveExistingInvolvements(Involvement involvement);
        ICollection<Sector> RetrieveFromCache(HttpContextBase context);
        InvolvementViewModel RetrieveFromSession(HttpSessionStateBase session);
        Involvement RetrieveInvolvement(Involvement involvement, HttpSessionStateBase session);
        List<Sector> RetrieveSectors();
        void Save(Involvement involvement);
    }
}