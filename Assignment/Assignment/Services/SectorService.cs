﻿using Assignment.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;

namespace Assignment.Services
{
    public class SectorService : ISectorService
    {
        private IAssignmentContext db;
        public SectorService(IAssignmentContext context)
        {
            db = context;
        }

        public List<Sector> RetrieveSectors()
        {
            var sectors = new List<Sector>();
            // Retrieve data one from db
            var lookup = db.Sectors.ToLookup(x => x.ParentId);

            var tree = CreateTree(lookup, null, 0);

            foreach (var item in tree)
            {
                var parts = item.Split('_');
                sectors.Add(new Sector() { Id = int.Parse(parts[1]), Name = parts[0] });
            }
            return sectors;
        }

        private IEnumerable<string> CreateTree(ILookup<int?, Sector> lookup, int? parent, int indent)
        {
            var line = from c in lookup[parent]
                           // Add 4 spaces before each inner level sector {name} and _{id} in the end
                       from t in new[] { new StringBuilder().Insert(0,  HttpUtility.HtmlDecode("&nbsp;"), indent * 4)
                           .ToString() + c.Name  +"_" + c.Id  }
                           .Concat(CreateTree(lookup, c.Id, indent + 1))
                       orderby c.Name
                       select t;

            return line;

        }

        public void AddSectorsToViewModel(InvolvementViewModel involvementViewModel, HttpContextBase context)
        {
            if (NotExistInCache(context))
            {
                involvementViewModel.Sectors = RetrieveSectors();
                AddToCache(context, involvementViewModel.Sectors);
            }
            else
            {
                involvementViewModel.Sectors = RetrieveFromCache(context);
            }
        }

        public virtual ICollection<Sector> RetrieveFromCache(HttpContextBase context)
        {
            return context.Application["Sectors"] as ICollection<Sector>;
        }

        public virtual bool NotExistInCache(HttpContextBase context)
        {
            return context.Application["Sectors"] == null;
        }

        public virtual void AddToCache(HttpContextBase context, ICollection<Sector> sectors)
        {
            context.Application["Sectors"] = sectors;
        }

        public virtual bool ExistsInSession(HttpSessionStateBase session)
        {
            return session["Involvement"] != null;
        }

        public virtual InvolvementViewModel RetrieveFromSession(HttpSessionStateBase session)
        {
            return session["Involvement"] as InvolvementViewModel;
        }

        public virtual void RemoveExistingInvolvements(Involvement involvement)
        {
            db.SectorInvolvements.RemoveRange(db.SectorInvolvements.Where(x => x.InvolvementId == involvement.Id));
        }

        public virtual void AddToCache(InvolvementViewModel involvementViewModel, HttpSessionStateBase session)
        {
            session["Involvement"] = involvementViewModel;
        }

        public virtual Involvement RetrieveInvolvement(Involvement involvement, HttpSessionStateBase session)
        {
            // If involvement exists in session, retrieve from db
            if (ExistsInSession(session))
            {
                var cachedInvolvement = RetrieveFromSession(session);
                if (cachedInvolvement.InvolvmentId > 0)
                {
                    involvement = db.Involvements.Find(cachedInvolvement.InvolvmentId);
                }
            }

            // If involvement does not exist, create new involvement
            if (involvement == null)
            {
                involvement = new Involvement();
            }

            return involvement;
        }

        public virtual void Save(Involvement involvement)
        {
            if (involvement.Id == 0)
                db.Involvements.Add(involvement);
            db.SaveChanges();
        }
    }
}