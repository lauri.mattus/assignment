﻿using Assignment.Services;
using Assignment.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace Assignement.Tests
{
    class TestSectorservice : SectorService
    {
        public bool InSession { get; set; }

        IAssignmentContext db;

        public TestSectorservice(IAssignmentContext context) : base(context)
        {
            this.db = context;
        }

        public override void AddToCache(HttpContextBase context, ICollection<Sector> sectors)
        {
            
        }
        public override bool NotExistInCache(HttpContextBase context)
        {
            return true;
        }

        public override ICollection<Sector> RetrieveFromCache(HttpContextBase context)
        {
            return null;
        }

        public override bool ExistsInSession(HttpSessionStateBase session)
        {
            return InSession;
        }

        public override InvolvementViewModel RetrieveFromSession(HttpSessionStateBase session)
        {
            if (InSession)
                return new InvolvementViewModel() { InvolvmentId = 1 };
            return null;
        }

        public override void AddToCache(InvolvementViewModel involvementViewModel, HttpSessionStateBase session)
        {

        }

        public override void RemoveExistingInvolvements(Involvement involvement)
        {
            foreach (var sector in db.SectorInvolvements.Where(x => x.InvolvementId == involvement.Id))
            {
                db.SectorInvolvements.Remove(sector);
            }
          
        }
    }
}
