﻿
using Assignment.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assignment.Tests
{
    class TestSectorDbSet : TestDbSet<Sector>
    {
        public override Sector Find(params object[] keyValues)
        {
            return this.SingleOrDefault(sector => sector.Id == (int)keyValues.Single());
        }
    }
}
