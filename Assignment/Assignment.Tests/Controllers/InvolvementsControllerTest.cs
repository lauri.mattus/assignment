﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Assignment;
using Assignment.Controllers;
using Assignment.Models;
using System.Net;
using Assignement.Tests;

namespace Assignment.Tests.Controllers
{
    [TestClass]
    public class InvolvementsControllerTest
    {
        [TestMethod]
        public void IndexGet()
        {
            // Arrange
            var context = new TestAssignmentContext();
            context.Sectors.Add(new Sector() { Id = 1, Name = "Manufacture", ParentId = null });
            context.Sectors.Add(new Sector() { Id = 2, Name = "Services", ParentId = null });
            context.Sectors.Add(new Sector() { Id = 3, Name = "Cars", ParentId = 1 });
            context.Sectors.Add(new Sector() { Id = 4, Name = "Sportcars", ParentId = 3 });

            var sectorService = new TestSectorservice(context);
            var controller = new InvolvementsController(sectorService);

            // Act
            var result = controller.Index() as ViewResult;

            // Assert
            Assert.IsNotNull(result);
            var model = result.ViewData.Model as InvolvementViewModel;
            Assert.IsNotNull(model);

            var sectors = model.Sectors as List<Sector>;
            Assert.IsNotNull(model);

            Assert.AreEqual("Manufacture", sectors[0].Name);
            Assert.AreEqual("    Cars", sectors[1].Name);
            Assert.AreEqual("        Sportcars", sectors[2].Name);
            Assert.AreEqual("Services", sectors[3].Name);
        }

        [TestMethod]
        public void IndexPost()
        {
            // Arrange
            var context = new TestAssignmentContext();
           
            var sectorService = new TestSectorservice(context);
            var controller = new InvolvementsController(sectorService);

            var involvementViewModel = new InvolvementViewModel()
            {
                InvolvmentId = 1,
                Name = "Peeter",
                AgreedToTerms = true,
                SelectedSectorIds = new int[] { 2, 4 }
            };

            // Act
            var result = controller.Index(involvementViewModel) as ViewResult;
            var involvement = context.Involvements.FirstOrDefault();

            // Assert
            Assert.IsNotNull(result);
            
            var model = result.ViewData.Model as InvolvementViewModel;

            Assert.IsNotNull(model);
            Assert.AreEqual("Saved successfully", controller.ViewData["Saved"] as string);

            Assert.IsNotNull(involvement);
            Assert.AreEqual("Peeter", involvement.Name);

            var sectorInvolvements = involvement.SectorInvolvements.ToList();
            Assert.AreEqual(2, sectorInvolvements.Count);
            Assert.AreEqual(2, sectorInvolvements[0].SectorId);
            Assert.AreEqual(4, sectorInvolvements[1].SectorId);
        }

        [TestMethod]
        public void IndexPostInvolvementsExists()
        {
            // Arrange
            var context = new TestAssignmentContext();

            var sectorService = new TestSectorservice(context);
            var controller = new InvolvementsController(sectorService);

            sectorService.InSession = true;
            var involvement = new Involvement()
            {
                Id = 1,
                Name = "Kalle",
                SectorInvolvements = new List<SectorInvolvement>
                {
                    new SectorInvolvement() { SectorId = 1, InvolvementId = 1 },
                    new SectorInvolvement() { SectorId = 3, InvolvementId = 1 }
                }
            };
            context.Involvements.Add(involvement);

            var involvementViewModel = new InvolvementViewModel()
            {
                InvolvmentId = 1,
                Name = "Peeter",
                AgreedToTerms = true,
                SelectedSectorIds = new int[] { 2, 4 }
            };

            // Act
            var result = controller.Index(involvementViewModel) as ViewResult;
            var changedInvolvement = context.Involvements.FirstOrDefault();

            // Assert
            Assert.IsNotNull(result);

            var model = result.ViewData.Model as InvolvementViewModel;

            Assert.IsNotNull(model);
            Assert.AreEqual("Saved successfully", controller.ViewData["Saved"] as string);

            Assert.IsNotNull(changedInvolvement);
            Assert.AreEqual("Peeter", changedInvolvement.Name);

            var sectorInvolvements = changedInvolvement.SectorInvolvements.ToList();
            Assert.AreEqual(2, sectorInvolvements.Count);
            Assert.AreEqual(2, sectorInvolvements[0].SectorId);
            Assert.AreEqual(4, sectorInvolvements[1].SectorId);
        }

        [TestMethod]
        public void IndexPostInvolvementsFailed()
        {
            // Arrange
            var context = new TestAssignmentContext();

            var sectorService = new TestSectorservice(context);
            var controller = new InvolvementsController(sectorService);

            var involvementViewModel = new InvolvementViewModel();

            // Act
            var result = controller.Index(involvementViewModel) as ViewResult;

            // Assert
            Assert.IsNotNull(result);
            Assert.AreEqual("Saving failed", controller.ViewData["Saved"] as string);
        }
    }
}
