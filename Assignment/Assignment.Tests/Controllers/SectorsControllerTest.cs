﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Assignment;
using Assignment.Controllers;
using Assignment.Models;
using System.Net;
using Assignement.Tests;

namespace Assignment.Tests.Controllers
{
    [TestClass]
    public class SectorsControllerTest
    {
        [TestMethod]
        public void Index()
        {
            // Arrange
            var context = new TestAssignmentContext();
            context.Sectors.Add(new Sector() { Id = 1, Name = "Manufacture", ParentId = null });
            context.Sectors.Add(new Sector() { Id = 2, Name = "Cars", ParentId = 1 });

            var sectorService = new TestSectorservice(context);
            var controller = new SectorsController(context, sectorService);

            // Act
            var result = controller.Index() as ViewResult;

            // Assert
            Assert.IsNotNull(result);
            Assert.IsInstanceOfType(result.ViewData.Model, typeof(List<Sector>));
            Assert.AreEqual(2, ((List<Sector>)result.ViewData.Model).Count);
        }

        [TestMethod]
        public void Details()
        {
            // Arrange
            var context = new TestAssignmentContext();
            context.Sectors.Add(new Sector() { Id = 1, Name = "Manufacture", ParentId = null });

            var sectorService = new TestSectorservice(context);
            var controller = new SectorsController(context, sectorService);
            
            // Act
            var result = controller.Details(1) as ViewResult;

            // Assert
            Assert.IsNotNull(result);
            Assert.IsInstanceOfType(result.ViewData.Model, typeof(Sector));
            var sector = (Sector)result.ViewData.Model;
            Assert.AreEqual(sector.Name, "Manufacture");
        }

        [TestMethod]
        public void DetailsSectorNull()
        {
            // Arrange
            var context = new TestAssignmentContext();
            context.Sectors.Add(new Sector() { Id = 1, Name = "Manufacture", ParentId = null });

            var sectorService = new TestSectorservice(context);
            var controller = new SectorsController(context, sectorService);

            // Act
            var result = controller.Details(null) as HttpStatusCodeResult;

            // Assert
            Assert.IsNotNull(result);
            Assert.AreEqual(result.StatusCode, (int)HttpStatusCode.BadRequest);
        }

        [TestMethod]
        public void DetailsSectorNotExists()
        {
            // Arrange
            var context = new TestAssignmentContext();
            context.Sectors.Add(new Sector() { Id = 1, Name = "Manufacture", ParentId = null });

            var sectorService = new TestSectorservice(context);
            var controller = new SectorsController(context, sectorService);

            // Act
            var result = controller.Details(2) as HttpNotFoundResult;

            // Assert
            Assert.IsNotNull(result);
        }

        [TestMethod]
        public void CreateGet()
        {
            // Arrange
            var context = new TestAssignmentContext();
            var sectorService = new TestSectorservice(context);
            var controller = new SectorsController(context, sectorService);

            // Act
            ViewResult result = controller.Create() as ViewResult;

            // Assert
            Assert.IsNotNull(result);
        }

        [TestMethod]
        public void CreatePost()
        {
            // Arrange
            var context = new TestAssignmentContext();
            var sectorService = new TestSectorservice(context);
            var controller = new SectorsController(context, sectorService);
            var sector = new Sector() { Id = 1, Name = "Services", ParentId = null };
            // Act
            var result = controller.Create(sector) as RedirectToRouteResult;
            var addedSector = context.Sectors.Find(1);
            // Assert
            Assert.IsNotNull(result);
            Assert.AreEqual("Index", result.RouteValues.Values.FirstOrDefault());
         
            Assert.IsNotNull(addedSector);
            Assert.AreEqual("Services", addedSector.Name);
        }

        [TestMethod]
        public void CreateSectorNotValid()
        {
            // Arrange
            var context = new TestAssignmentContext();
            var sectorService = new TestSectorservice(context);
            var controller = new SectorsController(context, sectorService);
            controller.ModelState.AddModelError("error", "has error");
            var sector = new Sector() { Id = 1, Name = "Services", ParentId = null };
            
            // Act
            ViewResult result = controller.Create(sector) as ViewResult;

            // Assert
            Assert.IsNotNull(result);
            var addedSector = context.Sectors.Find(1);
            Assert.IsNull(addedSector);
        }

        [TestMethod]
        public void EditGet()
        {
            // Arrange
            var context = new TestAssignmentContext();
            var sectorService = new TestSectorservice(context);
            var controller = new SectorsController(context, sectorService);
            var sector = new Sector() { Id = 1, Name = "Services", ParentId = null };
            context.Sectors.Add(sector);
            
            // Act
            ViewResult result = controller.Edit(1) as ViewResult;

            // Assert
            Assert.IsNotNull(result);
            Assert.IsInstanceOfType(result.ViewData.Model, typeof(Sector));
            var sectorFromView = (Sector)result.ViewData.Model;
            Assert.AreEqual(sector.Name, "Services");
        }

        [TestMethod]
        public void EditGetSectorNull()
        {
            // Arrange
            var context = new TestAssignmentContext();
            var sectorService = new TestSectorservice(context);
            var controller = new SectorsController(context, sectorService);
            var sector = new Sector() { Id = 1, Name = "Services", ParentId = null };
            context.Sectors.Add(sector);

            // Act
            var result = controller.Edit(null as int?) as HttpStatusCodeResult;

            // Assert
            Assert.IsNotNull(result);
            Assert.AreEqual(result.StatusCode, (int)HttpStatusCode.BadRequest);
        }

        [TestMethod]
        public void EditGetSectorNotExists()
        {
            // Arrange
            var context = new TestAssignmentContext();
            context.Sectors.Add(new Sector() { Id = 1, Name = "Manufacture", ParentId = null });

            var sectorService = new TestSectorservice(context);
            var controller = new SectorsController(context, sectorService);

            // Act
            var result = controller.Edit(2) as HttpNotFoundResult;

            // Assert
            Assert.IsNotNull(result);
        }
        [TestMethod]
        public void EditPost()
        {
            // Arrange
            var context = new TestAssignmentContext();
            var sectorService = new TestSectorservice(context);
            var controller = new SectorsController(context, sectorService);
            var sector = new Sector() { Id = 1, Name = "Services", ParentId = null };
            context.Sectors.Add(sector);

            var sectorForModel = new Sector() { Id = 1, Name = "Manufacture", ParentId = null };
            
            // Act
            var result = controller.Edit(sectorForModel) as RedirectToRouteResult;
            var changedSector = context.Sectors.Find(1);
            // Assert
            Assert.IsNotNull(result);
            Assert.AreEqual("Index", result.RouteValues.Values.FirstOrDefault());

            Assert.IsNotNull(changedSector);
            Assert.AreEqual("Manufacture", changedSector.Name);
        }


        [TestMethod]
        public void EditSectorNotValid()
        {
            // Arrange
            var context = new TestAssignmentContext();
            var sectorService = new TestSectorservice(context);
            var controller = new SectorsController(context, sectorService);
            controller.ModelState.AddModelError("error", "has error");
            var sector = new Sector() { Id = 1, Name = "Services", ParentId = null };
            context.Sectors.Add(sector);

            var sectorForModel = new Sector() { Id = 1, Name = "Manufacture", ParentId = null };

            // Act
            ViewResult result = controller.Edit(sectorForModel) as ViewResult;

            // Assert
            Assert.IsNotNull(result);
            var changedSector = context.Sectors.Find(1);
            Assert.IsNotNull(changedSector);
            Assert.AreEqual("Services", changedSector.Name);
        }

        [TestMethod]
        public void DeleteGet()
        {
            // Arrange
            var context = new TestAssignmentContext();
            var sectorService = new TestSectorservice(context);
            var controller = new SectorsController(context, sectorService);
            var sector = new Sector() { Id = 1, Name = "Services", ParentId = null };
            context.Sectors.Add(sector);
            // Act
            ViewResult result = controller.Delete(1) as ViewResult;

            // Assert
            Assert.IsNotNull(result);
        }

        [TestMethod]
        public void DeleteGetSectorNull()
        {
            // Arrange
            var context = new TestAssignmentContext();
            var sectorService = new TestSectorservice(context);
            var controller = new SectorsController(context, sectorService);
            var sector = new Sector() { Id = 1, Name = "Services", ParentId = null };
            context.Sectors.Add(sector);

            // Act
            var result = controller.Delete(null) as HttpStatusCodeResult;

            // Assert
            Assert.IsNotNull(result);
            Assert.AreEqual(result.StatusCode, (int)HttpStatusCode.BadRequest);
        }

        [TestMethod]
        public void DeleteGetSectorNotExists()
        {
            // Arrange
            var context = new TestAssignmentContext();
            context.Sectors.Add(new Sector() { Id = 1, Name = "Manufacture", ParentId = null });

            var sectorService = new TestSectorservice(context);
            var controller = new SectorsController(context, sectorService);

            // Act
            var result = controller.Delete(2) as HttpNotFoundResult;
            // Assert
            Assert.IsNotNull(result);
        }

        [TestMethod]
        public void DeleteGetSectorInvolvementExists()
        {
            // Arrange
            var context = new TestAssignmentContext();
            context.Sectors.Add(new Sector() { Id = 1, Name = "Manufacture", ParentId = null });
            context.SectorInvolvements.Add(new SectorInvolvement() { InvolvementId = 1, SectorId = 1 });

            var sectorService = new TestSectorservice(context);
            var controller = new SectorsController(context, sectorService);
            
            // Act
            var result = controller.Delete(1) as RedirectToRouteResult;

            // Assert
            Assert.IsNotNull(result);
            Assert.AreEqual("Cannot delete Sector that has involvements", controller.TempData["CannotDelete"]);
            Assert.AreEqual("Index", result.RouteValues.Values.FirstOrDefault());

        }

        [TestMethod]
        public void DeleteGetSectorChildSectorExists()
        {
            // Arrange
            var context = new TestAssignmentContext();
            context.Sectors.Add(new Sector() { Id = 1, Name = "Manufacture", ParentId = null });
            context.Sectors.Add(new Sector() { Id = 2, Name = "Metal", ParentId = 1 });

            var sectorService = new TestSectorservice(context);
            var controller = new SectorsController(context, sectorService);

            // Act
            var result = controller.Delete(1) as RedirectToRouteResult;

            // Assert
            Assert.IsNotNull(result);
            Assert.AreEqual("Cannot delete Sector that has Sub-Sectors", controller.TempData["CannotDelete"]);
            Assert.AreEqual("Index", result.RouteValues.Values.FirstOrDefault());

        }
    }
}
