﻿
using Assignment.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assignment.Tests
{
    public class TestAssignmentContext : IAssignmentContext
    {
        List<Sector> modifiedSectors = new List<Sector>();
        public TestAssignmentContext()
        {
            Sectors = new TestSectorDbSet();
            SectorInvolvements = new TestSectorInvolvementDbSet();
            Involvements = new TestInvolvemventDbSet();
        }

        public DbSet<Sector> Sectors { get; set; }
        public DbSet<SectorInvolvement> SectorInvolvements { get; set; }
        public DbSet<Involvement> Involvements { get; set; }

        public int SaveChanges()
        {
            if(modifiedSectors.Count> 0)
            {
                var modifiedIds = modifiedSectors.Select(y => y.Id).ToList(); ;
              
                foreach (var sector in Sectors.Where(x => modifiedIds.Contains(x.Id)).ToList())
                {
                    Sectors.Remove(sector);
                }

                foreach (var sector in modifiedSectors)
                {
                    Sectors.Add(sector);
                }
                
                modifiedSectors.Clear();
            }

            if(Involvements.ToList().Count > 0)
            {

            }
            
            return 0;
        }

        public void Dispose() { }
        public void MarkAsModified(Sector item)
        {
            modifiedSectors.Add(item);
        }
    }
}
