﻿
using Assignment.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assignment.Tests
{
    class TestInvolvemventDbSet : TestDbSet<Involvement>
    {
        public override Involvement Find(params object[] keyValues)
        {
            return this.SingleOrDefault(involvement => involvement.Id == (int)keyValues.Single());
        }
    }
}
